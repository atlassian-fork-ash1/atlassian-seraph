package com.atlassian.seraph.interceptor;

import com.atlassian.seraph.Initable;

/**
 * The base interface for Seraph interceptors. Interceptors allow you to run code before and after security events.
 */
public interface Interceptor extends Initable
{
    public void destroy();
}
