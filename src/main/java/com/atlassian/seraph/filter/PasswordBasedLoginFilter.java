package com.atlassian.seraph.filter;

import com.atlassian.seraph.auth.AuthenticatorException;

import static com.atlassian.seraph.auth.LoginReason.AUTHENTICATED_FAILED;
import static com.atlassian.seraph.auth.LoginReason.AUTHENTICATION_DENIED;
import static com.atlassian.seraph.auth.LoginReason.OK;

import com.atlassian.seraph.elevatedsecurity.ElevatedSecurityGuard;
import com.atlassian.seraph.interceptor.LoginInterceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This is a base filter that logs the user in based on the given username and password. It is designed to be extended
 * to support schemes that pass username and password one way or another.
 * <p>
 * For further info see superclass.
 *
 * @see com.atlassian.seraph.filter.BaseLoginFilter
 */
public abstract class PasswordBasedLoginFilter extends BaseLoginFilter
{
    private static final Logger log = LoggerFactory.getLogger(PasswordBasedLoginFilter.class);

    /**
     * This implements the login method defined in {@link com.atlassian.seraph.filter.BaseLoginFilter#login(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)}
     * and uses extracted user name and password to perform the check
     *
     * @param request the HTTP request in play
     * @param response the HTTP respone in play
     *
     * @return the login status string.
     *         <p>
     * The possible statuses are:
     * <ul>
     *  <li> LoginFilter.LOGIN_SUCCESS - the login was processed, and user was logged in</li>
     *  <li> LoginFilter.LOGIN_FAILURE - the login was processed, the user gave a bad username or password</li>
     *  <li> LoginFilter.LOGIN_ERROR - the login was processed, an exception occurred trying to log the user in</li>
     *  <li> LoginFilter.LOGIN_NOATTEMPT - the login was no processed, no form parameters existed</li>
     * </ul>
     */
    @Override
    public String login(final HttpServletRequest request, final HttpServletResponse response)
    {
        final String METHOD = "login : ";
        final boolean dbg = log.isDebugEnabled();

        String loginStatus = LOGIN_NOATTEMPT;

        // check for parameters
        final UserPasswordPair userPair = extractUserPasswordPair(request);
        if (userPair == null || userPair.userName == null || userPair.password == null)
        {
            if (dbg)
            {
                log.debug(METHOD + "No user name or password was returned. No authentication attempt will be made.  User may still be found via a SecurityFilter later.");
            }
            return loginStatus;
        }

        if (dbg)
        {
            log.debug(METHOD + "'" + userPair.userName + "' and password provided - remember me : " + userPair.persistentLogin + " - attempting login request");
        }

        final List<LoginInterceptor> interceptors = getSecurityConfig().getInterceptors(LoginInterceptor.class);
        runBeforeLoginInterceptors(request, response, userPair, interceptors);

        try
        {
            loginStatus = runAuthentication(request, response, userPair);
        }
        catch (final AuthenticatorException ex)
        {
            if (dbg)
            {
                log.debug(METHOD + "An exception occurred authenticating : '" + userPair.userName + "'", ex);
            }
            loginStatus = LOGIN_ERROR;
            // set the Error Type
            request.setAttribute(AUTHENTICATION_ERROR_TYPE, ex.getErrorType());
        }
        finally
        {
            runAfterLoginInterceptors(request, response, loginStatus, userPair, interceptors);
        }
        return loginStatus;
    }

    /**
     * This will check to see if an elevated security check is required and then perform it before then asking for the
     * authenticator and using it to authenticate/authorise the user.
     *
     * @param httpServletRequest  the HTTP servlet request in play
     * @param httpServletResponse the HTTP servlet respone in play
     * @param userPair            the {@link com.atlassian.seraph.filter.PasswordBasedLoginFilter.UserPasswordPair} in
     *                            play
     *
     * @return the status of the login
     *
     * @throws AuthenticatorException if something goes wrong in the authenticator
     */
    private String runAuthentication(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse, final UserPasswordPair userPair) throws AuthenticatorException
    {
        final String METHOD = "runAuthentication : ";
        final boolean dbg = log.isDebugEnabled();

        ElevatedSecurityGuard securityGuard = getElevatedSecurityGuard();

        if (! securityGuard.performElevatedSecurityCheck(httpServletRequest, userPair.userName))
        {
            if (dbg)
            {
                log.debug(METHOD + "'" + userPair.userName + "' failed elevated security check");
            }
            AUTHENTICATION_DENIED.stampRequestResponse(httpServletRequest,httpServletResponse);
            securityGuard.onFailedLoginAttempt(httpServletRequest, userPair.userName);
            return LOGIN_FAILED;
        }
        if (dbg)
        {
            log.debug(METHOD + "'" + userPair.userName + "' does not require elevated security check.  Attempting authentication...");
        }

        // ask the authenticator to try to log in the user the us
        final boolean loggedIn = getAuthenticator().login(httpServletRequest, httpServletResponse, userPair.userName, userPair.password, userPair.persistentLogin);
        if (dbg)
        {
            log.debug(METHOD + "'" + userPair.userName + "' was " + (loggedIn? "successfully" : "UNSUCCESSFULLY") + " authenticated");
        }

        if (loggedIn)
        {
            OK.stampRequestResponse(httpServletRequest,httpServletResponse);
            securityGuard.onSuccessfulLoginAttempt(httpServletRequest, userPair.userName);
        }
        else
        {
            AUTHENTICATED_FAILED.stampRequestResponse(httpServletRequest,httpServletResponse);
            securityGuard.onFailedLoginAttempt(httpServletRequest, userPair.userName);
        }
        return loggedIn ? LOGIN_SUCCESS : LOGIN_FAILED;
    }

    private void runBeforeLoginInterceptors(final HttpServletRequest request, final HttpServletResponse response, final UserPasswordPair userPair, final List<LoginInterceptor> interceptors)
    {
        for (final LoginInterceptor loginInterceptor : interceptors)
        {
            loginInterceptor.beforeLogin(request, response, userPair.userName, userPair.password, userPair.persistentLogin);
        }
    }

    private void runAfterLoginInterceptors(final HttpServletRequest request, final HttpServletResponse response, final String status, final UserPasswordPair userPair, final List<LoginInterceptor> interceptors)
    {
        for (final LoginInterceptor loginInterceptor : interceptors)
        {
            loginInterceptor.afterLogin(request, response, userPair.userName, userPair.password, userPair.persistentLogin, status);
        }
    }

    /**
     * Returns a username password pair for this request. If this request does not contain user credentials - returns
     * null;
     *
     * @param request the HTTP request in play
     * @return user credentials or null
     */
    protected abstract UserPasswordPair extractUserPasswordPair(HttpServletRequest request);

    /**
     * Represents a username password pair of user credentials.
     */
    public static final class UserPasswordPair
    {
        final String userName;
        final String password;
        final boolean persistentLogin;

        public UserPasswordPair(final String user, final String password, final boolean persistentLogin)
        {
            userName = user;
            this.password = password;
            this.persistentLogin = persistentLogin;
        }
    }
}
