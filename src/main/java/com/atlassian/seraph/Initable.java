package com.atlassian.seraph;

import com.atlassian.seraph.config.SecurityConfig;

import java.util.Map;

/**
 * A simple interface to indicate all initialisable configuration classes in Seraph.
 */
public interface Initable
{
    /**
     * Initialise the configuration object with the given "init-params".
     *
     * @param params The map of "init-params" extracted from the Seraph config file. This is guaranteed not null.
     * @param config The Seraph SecurityConfig class that is initialising the config objects. This object will only be partially initialised at this time.
     */
    void init(Map<String, String> params, SecurityConfig config);
}
