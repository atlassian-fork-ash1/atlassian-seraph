package com.atlassian.seraph.service.rememberme;

/**
 * An implementation of {@link RememberMeToken} that should be suitable for most apps
 *
 * Have a look at the {@link #builder(String)} methods to see how you can costruct them easily enough
 */
public class DefaultRememberMeToken implements RememberMeToken
{
    private final Long id;
    private final String randomString;
    private final String userName;
    private final long createdTime;


    private DefaultRememberMeToken(final Long id, final String randomString, final String userName, final long createdTime)
    {
        this.id = id;
        this.randomString = randomString;
        this.userName = userName;
        this.createdTime = createdTime;
    }

    @Override
    public Long getId()
    {
        return id;
    }

    @Override
    public String getRandomString()
    {
        return randomString;
    }

    @Override
    public String getUserName()
    {
        return userName;
    }

    @Override
    public long getCreatedTime()
    {
        return createdTime;
    }

    public static Builder builder(final Long id, final String randomString)
    {
        return new Builder(id, randomString);
    }

    public static Builder builder(final String randomString)
    {
        return new Builder(randomString);
    }

    public static Builder builder(final RememberMeToken token)
    {
        return new Builder(token);
    }

    public static class Builder
    {

        private Long id;
        private String randomString;
        private String userName;
        private long createdTime;

        public Builder(final RememberMeToken token)
        {
            this.id = token.getId();
            this.randomString = token.getRandomString();
            this.userName = token.getUserName();
            this.createdTime = token.getCreatedTime();

        }

        public Builder(final Long id, final String randomString)
        {
            this.id = id;
            this.randomString = randomString;
        }

        public Builder(final String randomString)
        {
            this.randomString = randomString;
        }

        public Builder setId(Long id)
        {
            this.id = id;
            return this;
        }

        public Builder setUserName(String userName)
        {
            this.userName = userName;
            return this;
        }

        public Builder setRandomString(String randomString)
        {
            this.randomString = randomString;
            return this;
        }

        public Builder setCreatedTime(long createdTime)
        {
            this.createdTime = createdTime;
            return this;
        }

        public RememberMeToken build()
        {
            return new DefaultRememberMeToken(id, randomString, userName, createdTime);
        }
    }
}
