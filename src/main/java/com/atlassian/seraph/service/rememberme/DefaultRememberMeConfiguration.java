package com.atlassian.seraph.service.rememberme;

import com.atlassian.seraph.config.SecurityConfig;
import com.atlassian.seraph.config.SecurityConfigFactory;
import com.atlassian.seraph.spi.rememberme.RememberMeConfiguration;
import com.atlassian.seraph.util.ServerInformationParser;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import static com.atlassian.seraph.util.ServerInformationParser.ServerInformation;
import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * This default implementation of {@link com.atlassian.seraph.spi.rememberme.RememberMeConfiguration}
 */
public class DefaultRememberMeConfiguration implements RememberMeConfiguration
{
    public static final int TWO_WEEKS = 2 * 7 * 24 * 60 * 60;
    private final SecurityConfig config;


    public DefaultRememberMeConfiguration()
    {
        this(SecurityConfigFactory.getInstance());
    }

    public DefaultRememberMeConfiguration(final SecurityConfig config)
    {
        this.config = config;
    }

    /**
     * This app may needs to determine this.  See JRA-10508.  By default we load it from the same place that Serpah uses
     * for backwards compatibility.
     *
     * @return true if {@link javax.servlet.http.Cookie#setSecure(boolean)} should be called with true
     */
    @Override
    public boolean isInsecureCookieAlwaysUsed()
    {
        return config.isInsecureCookie();
    }


    /**
     * By default we take the conservative route and not use HttpOnly cookies.  However an application
     * can decide to make it more secure and return true.  We had initial troubles in JIRA and hence
     * we took the conservative route.  Eventually we want to get around these problems 
     * 
     * @return
     */
    @Override
    public boolean isCookieHttpOnly(HttpServletRequest httpServletRequest)
    {
        ServletContext servletContext = httpServletRequest.getSession().getServletContext();

        try
        {
            ServerInformation serverInfo = ServerInformationParser.parse(servletContext.getServerInfo());

            // Apache Tomcat versions 5.5.28+ and 6.0.19+ support httpOnly cookies
            // All containers supporting Servlet API 3.0 support httpOnly cookies
            final boolean servletApiSupportsHttpOnlyCookies = servletContext.getMajorVersion() >= 3;
            if (servletApiSupportsHttpOnlyCookies)
            {
                return true;
            }

            return serverInfo.isApacheTomcat() &&
                ((serverInfo.getVersion().startsWith("5") && serverInfo.getVersion().compareTo("5.5.28") >= 0) ||
                 (serverInfo.getVersion().startsWith("6") && serverInfo.getVersion().compareTo("6.0.19") >= 0) );
        }
        catch (IllegalArgumentException e)
        {
            return false;
        }
    }

    @Override
    public String getCookieName()
    {
        return config.getLoginCookieKey();
    }

    @Override
    public int getCookieMaxAgeInSeconds()
    {
        int maxAge = config.getAutoLoginCookieAge();
        if (maxAge <= 0)
        {
            maxAge = TWO_WEEKS;
        }
        return maxAge;
    }

    @Override
    public String getCookieDomain(final HttpServletRequest httpServletRequest)
    {
        return null;
    }

    @Override
    public String getCookiePath(final HttpServletRequest httpServletRequest)
    {
        final String path = config.getLoginCookiePath();
        if (path != null)
        {
            return path;
        }
        final String contextPath = httpServletRequest.getContextPath();
        if (isBlank(contextPath))
        {
            return "/";
        }
        return contextPath;
    }
}
