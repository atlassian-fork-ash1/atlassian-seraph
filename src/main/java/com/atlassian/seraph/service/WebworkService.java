package com.atlassian.seraph.service;

import com.atlassian.seraph.SecurityService;
import com.atlassian.seraph.config.SecurityConfig;
import com.atlassian.seraph.util.CachedPathMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.opensymphony.util.ClassLoaderUtil;

import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Configures Seraph based on WebWork 1.x configuration file actions.xml. <p> Takes two optional init parameters:
 * <dl>
 * <dt>action.extension (default: "action")</dt>
 * <dd>The extension of action URLs which are intercepted by this service. If this parameter is not specified, the default is the WebWork default
 * 'action'. (This should match your servlet mapping in web.xml.)</dd>
 * <dt>actions.xml.file (default: "actions")</dt>
 * <dd>The location on the classpath of your actions.xml file (WebWork 1.x style). If this parameter is not specified, the default is the WebWork 1.x
 * default 'actions'. (This should match the value configured for 'webwork.configuration.xml' in webwork.properties.)</dd>
 * </dl>
 * In actions.xml (or other actions XML file, as specified by the init-param) specify roles required per action or command:
 *
 * <pre>
 *   &lt;action name=&quot;project.ViewProject&quot; alias=&quot;ViewProject&quot; roles-required=&quot;RoleFoo, RoleBar&quot;&gt;
 * or
 *   &lt;command name=&quot;Delete&quot; alias=&quot;DeleteProject&quot; roles-required=&quot;RoleBat&quot;&gt;
 * </pre>
 *
 * Roles can be separated by commas and spaces.
 */
public class WebworkService implements SecurityService
{
    private static final Logger log = LoggerFactory.getLogger(WebworkService.class);
    private static final String ROLES_REQUIRED_ATTR = "roles-required";
    private static final String ACTION_EXTENSION_INIT_PARAM = "action.extension";
    private static final String ACTIONS_XML_FILE_INIT_PARAM = "actions.xml.file";

    // Same as the WebWork default (i.e. action URLs end with '.action')
    private static final String ACTION_EXTENSION_DEFAULT = "action";

    // Same as the WebWork 1.x default (i.e. actions.xml is the mapping file)
    private static final String ACTIONS_XML_FILE_DEFAULT = "actions";

    // used to check which actions match the current path
    // This class only uses the "get" method of the pathmapper, so initialise the map
    // that caches its results to a large value. The getAll method of the PathMapper is not used
    // by this class so make the second caching map small.
    private final CachedPathMapper actionMapper = new CachedPathMapper();

    // maps current action to roles required
    private final Map<String, String> rolesMap = new ConcurrentHashMap<String, String>();

    @Override
    public void init(final Map<String, String> params, final SecurityConfig config)
    {
        try
        {
            // the extension of webwork actions (should match web.xml servlet mapping)
            String extension = params.get(ACTION_EXTENSION_INIT_PARAM);
            if (extension == null)
            {
                extension = ACTION_EXTENSION_DEFAULT;
            }

            // the XML file containing the action mappings (should match 'webwork.configuration.xml' property)

            String actionsXmlFile = params.get(ACTIONS_XML_FILE_INIT_PARAM);
            if (actionsXmlFile == null)
            {
                actionsXmlFile = ACTIONS_XML_FILE_DEFAULT;
            }

            configureActionMapper(extension, actionsXmlFile);
        }
        catch (final RuntimeException e)
        {
            log.error("Failed to initialise WebworkService", e);
        }
    }

    private void configureActionMapper(final String extension, final String actionsXmlFile)
    {
        final Document doc = parseActionsXmlFile(actionsXmlFile);

        // Get list of actions
        final NodeList actions = doc.getElementsByTagName("action");

        final String rootRolesRequired = overrideRoles(null, doc.getDocumentElement());

        final Map<String, String> pathMaps = new HashMap<String, String>();

        // Build list of views
        for (int i = 0; i < actions.getLength(); i++)
        {
            final Element action = (Element) actions.item(i);
            final String actionName = action.getAttribute("name");
            final String actionAlias = action.getAttribute("alias");
            final String actionRolesRequired = overrideRoles(rootRolesRequired, action);

            if (actionRolesRequired != null)
            {

                if (actionAlias != null)
                {
                    pathMaps.put(actionAlias, "/" + actionAlias + "." + extension);
                    rolesMap.put(actionAlias, actionRolesRequired);
                    pathMaps.put(actionAlias + "!*", "/" + actionAlias + "!*." + extension);
                    rolesMap.put(actionAlias + "!*", actionRolesRequired);
                }

                if (actionName != null)
                {
                    pathMaps.put(actionName, "/" + actionName + "." + extension);
                    rolesMap.put(actionName, actionRolesRequired);
                    pathMaps.put(actionName + "!*", "/" + actionName + "!*." + extension);
                    rolesMap.put(actionName + "!*", actionRolesRequired);
                }
            }

            // Get list of commands
            final NodeList commands = action.getElementsByTagName("command");
            for (int j = 0; j < commands.getLength(); j++)
            {
                final Element command = (Element) commands.item(j);
                final String cmdRolesRequired = overrideRoles(actionRolesRequired, command);

                final String commandAlias = command.getAttribute("alias");

                if ((commandAlias != null) && (cmdRolesRequired != null))
                {
                    pathMaps.put(commandAlias, "/" + commandAlias + "." + extension);
                    rolesMap.put(commandAlias, cmdRolesRequired);
                }
            }
        }
        actionMapper.set(pathMaps);
    }

    private Document parseActionsXmlFile(final String actionsXmlFile)
    {
        final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        URL fileUrl = ClassLoaderUtil.getResource(actionsXmlFile + ".xml", this.getClass());

        if (fileUrl == null)
        {
            fileUrl = ClassLoaderUtil.getResource("/" + actionsXmlFile + ".xml", this.getClass());
        }

        if (fileUrl == null)
        {
            throw new IllegalArgumentException("No such XML file:/" + actionsXmlFile + ".xml");
        }

        try
        {
            return factory.newDocumentBuilder().parse(fileUrl.toString());
        }
        catch (final SAXException e)
        {
            throw new RuntimeException(e);
        }
        catch (final IOException e)
        {
            throw new RuntimeException(e);
        }
        catch (final ParserConfigurationException e)
        {
            throw new RuntimeException(e);
        }
    }

    /**
     * Returns newRolesRequired if it isn't empty, and rolesRequired otherwise.
     */
    private String overrideRoles(final String rolesRequired, final Element action)
    {
        if (action.hasAttribute(ROLES_REQUIRED_ATTR))
        {
            return action.getAttribute(ROLES_REQUIRED_ATTR);
        }
        else
        {
            return rolesRequired;
        }
    }

    @Override
    public void destroy()
    {}

    @Override
    public Set<String> getRequiredRoles(final HttpServletRequest request)
    {
        final Set<String> requiredRoles = new HashSet<String>();

        final String currentURL = request.getRequestURI();

        final int lastSlash = currentURL.lastIndexOf('/');
        String targetURL;

        // then check webwork mappings
        if (lastSlash > -1)
        {
            targetURL = currentURL.substring(lastSlash);
        }
        else
        {
            targetURL = currentURL;
        }

        final String actionMatch = actionMapper.get(targetURL);

        if (actionMatch != null)
        {
            final String rolesStr = rolesMap.get(actionMatch);

            final StringTokenizer st = new StringTokenizer(rolesStr, ", ");
            while (st.hasMoreTokens())
            {
                requiredRoles.add(st.nextToken());
            }
        }

        return Collections.unmodifiableSet(requiredRoles);
    }
}
