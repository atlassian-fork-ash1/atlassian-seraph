package com.atlassian.seraph.spi.rememberme;

import javax.servlet.http.HttpServletRequest;

/**
 * This interface provides the information to allow the {@link com.atlassian.seraph.service.rememberme.RememberMeService}
 * to know what the name of the remember me cookie could be and how old it should be made and what path it should be
 * placed on and so on.
 */
public interface RememberMeConfiguration
{

    /**
     * @return the name of the cookie to look into for remember me information
     */
    String getCookieName();

    /**
     * @return the age in seconds for the remember me cookie
     */
    int getCookieMaxAgeInSeconds();

    /**
     * @param httpServletRequest the request in play
     *
     * @return the domain that should be used when writing the remember me cookie
     */
    String getCookieDomain(HttpServletRequest httpServletRequest);

    /**
     * @param httpServletRequest the request in play
     *
     * @return the path that should be used when writing the remember me cookie
     */
    String getCookiePath(HttpServletRequest httpServletRequest);

    /**
     * See JRA-10508
     *
     * @return true if the cookie should never be set to secure according to the request
     */
    boolean isInsecureCookieAlwaysUsed();

    /**
     * If this is true than Seraph will make the remember me a HttpOnly cookie.  Not all applications,
     * browsers and testing framework can handle HttpOnly cookies
     *
     * @param httpServletRequest the request in play
     * 
     * @return true for HttpOnly cookies
     */
    boolean isCookieHttpOnly(HttpServletRequest httpServletRequest);
}
