package mock;

import java.security.Principal;

/**
 * A simple mock principal
 */
public class MockPrincipal implements Principal
{

    private final String name;

    public MockPrincipal(String name)
    {
        this.name = name;
    }

    @Override
    public String getName()
    {
        return name;
    }
}
