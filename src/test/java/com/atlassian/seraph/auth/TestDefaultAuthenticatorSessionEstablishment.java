package com.atlassian.seraph.auth;

import com.atlassian.seraph.config.SecurityConfig;
import com.atlassian.seraph.filter.MockSecurityConfig;
import junit.framework.TestCase;
import mock.MockHttpRequest;
import mock.MockHttpResponse;
import mock.MockPrincipal;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

/**
 */
public class TestDefaultAuthenticatorSessionEstablishment extends TestCase
{

    private MockPrincipal principalFred;
    private MockPrincipal principalBill;
    private MockSession httpSession;
    private MockHttpRequest httpRequest;
    private MockHttpResponse httpResponse;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        principalFred = new MockPrincipal("fred");
        principalBill = new MockPrincipal("bill");
        httpSession = new MockSession(false);
        httpRequest = new MockHttpRequest(httpSession);
        httpResponse = new MockHttpResponse();
    }

    public void testSessionContext_NeedsTearDown_NoOneInSession()
    {
        StubAuthenticator authenticator = new StubAuthenticator(new StubSecurityConfig());
        authenticator.authoriseUserAndEstablishSession(httpRequest, httpResponse, principalFred);

        assertEquals(1, httpSession.getInvalidateCount());
    }

    public void testSessionContext_NeedsTearDown_DifferentPrincipalInSession()
    {
        httpSession.setAttribute(DefaultAuthenticator.LOGGED_IN_KEY, principalBill);

        StubAuthenticator authenticator = new StubAuthenticator(new StubSecurityConfig());
        authenticator.authoriseUserAndEstablishSession(httpRequest, httpResponse, principalFred);

        assertEquals(1, httpSession.getInvalidateCount());
    }

    public void testSessionContext_DoesNotNeedTearDown_PrincipalInSession()
    {
        MockPrincipal principalOtherFred = new MockPrincipal("fred");
        httpSession.setAttribute(DefaultAuthenticator.LOGGED_IN_KEY, principalOtherFred);

        StubAuthenticator authenticator = new StubAuthenticator(new StubSecurityConfig());
        authenticator.authoriseUserAndEstablishSession(httpRequest, httpResponse, principalFred);

        assertEquals(0, httpSession.getInvalidateCount());
    }

    private static class StubAuthenticator extends DefaultAuthenticator
    {
        SecurityConfig securityConfig;

        private StubAuthenticator(SecurityConfig securityConfig)
        {
            this.securityConfig = securityConfig;
        }

        @Override
        protected boolean isAuthorised(HttpServletRequest httpServletRequest, Principal principal)
        {
            return true;
        }

        @Override
        protected SecurityConfig getConfig()
        {
            return securityConfig;
        }

        @Override
        protected Principal getUser(String username)
        {
            return null;
        }

        @Override
        protected boolean authenticate(Principal user, String password) throws AuthenticatorException
        {
            return false;
        }
    }

    private static class StubSecurityConfig extends MockSecurityConfig
    {
        @Override
        public boolean isInvalidateSessionOnLogin()
        {
            return true;
        }
    }

}
