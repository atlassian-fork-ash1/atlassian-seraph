package com.atlassian.seraph.auth;

import com.atlassian.seraph.config.SecurityConfig;

import java.security.Principal;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * A Mock Authenticator.
 *
 * Currently it is only used to test loading config, so does not need any actual implementation.
 */
public class MockAuthenticator implements Authenticator
{
    @Override
    public void init(final Map<String, String> params, final SecurityConfig config)
    {
        // do nothing
    }

    @Override
    public void destroy()
    {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    @Override
    public String getRemoteUser(final HttpServletRequest request)
    {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    @Override
    public Principal getUser(final HttpServletRequest request)
    {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    @Override
    public Principal getUser(final HttpServletRequest request, final HttpServletResponse response)
    {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    @Override
    public boolean login(final HttpServletRequest request, final HttpServletResponse response, final String username, final String password)
            throws AuthenticatorException
    {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    @Override
    public boolean login(final HttpServletRequest request, final HttpServletResponse response, final String username, final String password, final boolean storeCookie)
            throws AuthenticatorException
    {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    @Override
    public boolean logout(final HttpServletRequest request, final HttpServletResponse response)
            throws AuthenticatorException
    {
        throw new UnsupportedOperationException("Not implemented yet.");
    }
}
