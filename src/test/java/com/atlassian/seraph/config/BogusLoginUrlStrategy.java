package com.atlassian.seraph.config;

import java.util.Map;

/**
 *
 */
public class BogusLoginUrlStrategy implements LoginUrlStrategy
{
    private String prefix;

    @Override
    public void init(final Map<String, String> params, final SecurityConfig config)
    {
        prefix = params.get("prefix");
    }

    @Override
    public String getLoginURL(final SecurityConfig config, final String configuredLoginUrl)
    {
        return prefix + configuredLoginUrl;
    }

    @Override
    public String getLogoutURL(final SecurityConfig config, final String configuredLogoutUrl)
    {
        return prefix + configuredLogoutUrl;
    }

    @Override
    public String getLinkLoginURL(final SecurityConfig config, final String configuredLinkLoginUrl)
    {
        return prefix + configuredLinkLoginUrl;
    }
}
