package com.atlassian.seraph.controller;

import junit.framework.TestCase;

public class TestNullSecurityController extends TestCase
{
    public TestNullSecurityController(String string)
    {
        super(string);
    }

    public void testAlwaysOn()
    {
        SecurityController controller = new NullSecurityController();
        assertTrue(controller.isSecurityEnabled());
    }
}
