package com.atlassian.seraph.filter;

import com.atlassian.seraph.auth.AuthenticationErrorType;
import com.atlassian.seraph.auth.Authenticator;
import com.atlassian.seraph.auth.AuthenticatorException;
import com.atlassian.seraph.config.SecurityConfig;
import com.atlassian.seraph.elevatedsecurity.CountingElevatedSecurityGuard;
import com.atlassian.seraph.interceptor.LoginInterceptor;
import com.atlassian.seraph.util.LocalMockHttpServletRequest;
import com.mockobjects.dynamic.C;
import com.mockobjects.dynamic.Mock;
import junit.framework.TestCase;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Tests for {@link com.atlassian.seraph.filter.PasswordBasedLoginFilter}
 */
public class TestPasswordBasedLoginFilter extends TestCase
{
    public void testNullUserNameFails()
    {
        final MockSecurityConfig securityConfig = new MockSecurityConfig(Collections.EMPTY_LIST);
        final PasswordBasedLoginFilter passwordBasedLoginFilter = new TestedPasswordBasedLoginFilter(null, "password", securityConfig);

        final String actualStatus = passwordBasedLoginFilter.login(makeHttpRequest(), makeHttpResponse());
        assertEquals(PasswordBasedLoginFilter.LOGIN_NOATTEMPT, actualStatus);
    }


    public void testNullPasswordFails()
    {
        final MockSecurityConfig securityConfig = new MockSecurityConfig(Collections.EMPTY_LIST);
        final PasswordBasedLoginFilter passwordBasedLoginFilter = new TestedPasswordBasedLoginFilter("userName", null, securityConfig);

        final String actualStatus = passwordBasedLoginFilter.login(makeHttpRequest(), makeHttpResponse());
        assertEquals(PasswordBasedLoginFilter.LOGIN_NOATTEMPT, actualStatus);
    }

    public void testNullUserPairFails()
    {
        final PasswordBasedLoginFilter passwordBasedLoginFilter = new PasswordBasedLoginFilter()
        {
            @Override
            protected PasswordBasedLoginFilter.UserPasswordPair extractUserPasswordPair(final HttpServletRequest request)
            {
                return null;
            }
        };

        final String actualStatus = passwordBasedLoginFilter.login(makeHttpRequest(), makeHttpResponse());
        assertEquals(PasswordBasedLoginFilter.LOGIN_NOATTEMPT, actualStatus);
    }

    public void testElevatedSecurityGuard_FailedCheck()
    {
        final CountingElevatedSecurityGuard securityGuard = new CountingElevatedSecurityGuard(false, "userName");

        final MockSecurityConfig securityConfig = new MockSecurityConfig(securityGuard, null, null, Collections.EMPTY_LIST);
        final PasswordBasedLoginFilter passwordBasedLoginFilter = new TestedPasswordBasedLoginFilter("userName", "password", securityConfig);


        final String actualStatus = passwordBasedLoginFilter.login(makeHttpRequest(), makeHttpResponse());
        assertEquals(PasswordBasedLoginFilter.LOGIN_FAILED, actualStatus);
        assertEquals(1, securityGuard.getFailedCount());
        assertEquals(0, securityGuard.getSuccessCount());
    }

    public void testElevatedSecurityGuard_PassedCheck_ButFailedAuthenticator()
    {
        final CountingElevatedSecurityGuard securityGuard = new CountingElevatedSecurityGuard(true, "userName");

        final AssertingAuthenticator authenticator = new AssertingAuthenticator(false, "userName", "password");

        final MockSecurityConfig securityConfig = new MockSecurityConfig(securityGuard, authenticator, null, Collections.EMPTY_LIST);
        final PasswordBasedLoginFilter passwordBasedLoginFilter = new TestedPasswordBasedLoginFilter("userName", "password", securityConfig);


        final String actualStatus = passwordBasedLoginFilter.login(makeHttpRequest(), makeHttpResponse());
        assertEquals(PasswordBasedLoginFilter.LOGIN_FAILED, actualStatus);
        assertEquals(1, securityGuard.getFailedCount());
        assertEquals(0, securityGuard.getSuccessCount());
    }

    public void testElevatedSecurityGuard_PassedCheck_ButAuthenticatorThrewException()
    {
        final CountingElevatedSecurityGuard securityGuard = new CountingElevatedSecurityGuard(true, "userName");

        @SuppressWarnings({"ThrowableInstanceNeverThrown"})
        final AssertingAuthenticator authenticator = new AssertingAuthenticator(false, "userName", "password", new AuthenticatorException());

        final MockSecurityConfig securityConfig = new MockSecurityConfig(securityGuard, authenticator, null, Collections.EMPTY_LIST);
        final PasswordBasedLoginFilter passwordBasedLoginFilter = new TestedPasswordBasedLoginFilter("userName", "password", securityConfig);


        final String actualStatus = passwordBasedLoginFilter.login(makeHttpRequest(), makeHttpResponse());
        assertEquals(PasswordBasedLoginFilter.LOGIN_ERROR, actualStatus);

        // we dont run the security guard on exception!  Its not really a fail against the user
        assertEquals(0, securityGuard.getFailedCount());
        assertEquals(0, securityGuard.getSuccessCount());
    }

    public void testElevatedSecurityGuard_PassedCheck_AndPassedAuthenticator()
    {
        final CountingElevatedSecurityGuard securityGuard = new CountingElevatedSecurityGuard(true, "userName");

        final AssertingAuthenticator authenticator = new AssertingAuthenticator(true, "userName", "password");
        final MockSecurityConfig securityConfig = new MockSecurityConfig(securityGuard, authenticator, null, Collections.EMPTY_LIST);
        final PasswordBasedLoginFilter passwordBasedLoginFilter = new TestedPasswordBasedLoginFilter("userName", "password", securityConfig);


        final String actualStatus = passwordBasedLoginFilter.login(makeHttpRequest(), makeHttpResponse());
        assertEquals(PasswordBasedLoginFilter.LOGIN_SUCCESS, actualStatus);
        assertEquals(0, securityGuard.getFailedCount());
        assertEquals(1, securityGuard.getSuccessCount());
    }


    public void testInterceptorsAreCalled()
    {
        CountingLoginInterceptor loginInterceptor = new CountingLoginInterceptor();
        List<CountingLoginInterceptor> interceptors = new ArrayList<CountingLoginInterceptor>();
        interceptors.add(loginInterceptor);

        final CountingElevatedSecurityGuard securityGuard = new CountingElevatedSecurityGuard(true, "userName");

        final AssertingAuthenticator authenticator = new AssertingAuthenticator(true, "userName", "password", null);

        final MockSecurityConfig securityConfig = new MockSecurityConfig(securityGuard, authenticator, null, interceptors);
        final PasswordBasedLoginFilter passwordBasedLoginFilter = new TestedPasswordBasedLoginFilter("userName", "password", securityConfig);


        passwordBasedLoginFilter.login(makeHttpRequest(), makeHttpResponse());
        assertEquals(1, loginInterceptor.getBeforeLogin());
        assertEquals(1, loginInterceptor.getAfterLogin());

    }

    public void testLoginNullErrorType()
    {
        final CountingElevatedSecurityGuard securityGuard = new CountingElevatedSecurityGuard(true, "userName");

        @SuppressWarnings({"ThrowableInstanceNeverThrown"})
        final AssertingAuthenticator authenticator = new AssertingAuthenticator(false, "userName", "password", new AuthenticatorException());

        final MockSecurityConfig securityConfig = new MockSecurityConfig(securityGuard, authenticator, null, Collections.EMPTY_LIST);
        final PasswordBasedLoginFilter passwordBasedLoginFilter = new TestedPasswordBasedLoginFilter("userName", "password", securityConfig);

        final LocalMockHttpServletRequest mockHttpServletRequest = makeHttpRequest();
        final String actualStatus = passwordBasedLoginFilter.login(mockHttpServletRequest, makeHttpResponse());
        assertEquals(PasswordBasedLoginFilter.LOGIN_ERROR, actualStatus);
        assertEquals(null, mockHttpServletRequest.getAttribute(BaseLoginFilter.AUTHENTICATION_ERROR_TYPE));

        // we dont run the security guard on exception!  Its not really a fail against the user
        assertEquals(0, securityGuard.getFailedCount());
        assertEquals(0, securityGuard.getSuccessCount());
    }

    public void testLoginCommunicationError()
    {
        final CountingElevatedSecurityGuard securityGuard = new CountingElevatedSecurityGuard(true, "userName");

        @SuppressWarnings({"ThrowableInstanceNeverThrown"})
        final AssertingAuthenticator authenticator = new AssertingAuthenticator(false, "userName", "password", new AuthenticatorException(AuthenticationErrorType.CommunicationError));

        final MockSecurityConfig securityConfig = new MockSecurityConfig(securityGuard, authenticator, null, Collections.EMPTY_LIST);
        final PasswordBasedLoginFilter passwordBasedLoginFilter = new TestedPasswordBasedLoginFilter("userName", "password", securityConfig);

        final LocalMockHttpServletRequest mockHttpServletRequest = makeHttpRequest();
        final String actualStatus = passwordBasedLoginFilter.login(mockHttpServletRequest, makeHttpResponse());
        assertEquals(PasswordBasedLoginFilter.LOGIN_ERROR, actualStatus);
        assertEquals(AuthenticationErrorType.CommunicationError, mockHttpServletRequest.getAttribute(BaseLoginFilter.AUTHENTICATION_ERROR_TYPE));

        // we dont run the security guard on exception!  Its not really a fail against the user
        assertEquals(0, securityGuard.getFailedCount());
        assertEquals(0, securityGuard.getSuccessCount());
    }

    public void testLoginUnknownError()
    {
        final CountingElevatedSecurityGuard securityGuard = new CountingElevatedSecurityGuard(true, "userName");

        @SuppressWarnings({"ThrowableInstanceNeverThrown"})
        final AssertingAuthenticator authenticator = new AssertingAuthenticator(false, "userName", "password", new AuthenticatorException(AuthenticationErrorType.UnknownError));

        final MockSecurityConfig securityConfig = new MockSecurityConfig(securityGuard, authenticator, null, Collections.EMPTY_LIST);
        final PasswordBasedLoginFilter passwordBasedLoginFilter = new TestedPasswordBasedLoginFilter("userName", "password", securityConfig);

        final LocalMockHttpServletRequest mockHttpServletRequest = makeHttpRequest();
        final String actualStatus = passwordBasedLoginFilter.login(mockHttpServletRequest, makeHttpResponse());
        assertEquals(PasswordBasedLoginFilter.LOGIN_ERROR, actualStatus);
        assertEquals(AuthenticationErrorType.UnknownError, mockHttpServletRequest.getAttribute(BaseLoginFilter.AUTHENTICATION_ERROR_TYPE));

        // we dont run the security guard on exception!  Its not really a fail against the user
        assertEquals(0, securityGuard.getFailedCount());
        assertEquals(0, securityGuard.getSuccessCount());
    }

    class TestedPasswordBasedLoginFilter extends PasswordBasedLoginFilter
    {
        private final String userName;
        private final String password;
        private final MockSecurityConfig mockSecurityConfig;


        TestedPasswordBasedLoginFilter(final String userName, final String password, final MockSecurityConfig mockSecurityConfig)
        {
            this.userName = userName;
            this.password = password;
            this.mockSecurityConfig = mockSecurityConfig;
        }

        @Override
        protected PasswordBasedLoginFilter.UserPasswordPair extractUserPasswordPair(final HttpServletRequest request)
        {
            return new PasswordBasedLoginFilter.UserPasswordPair(userName, password, true);
        }

        @Override
        protected SecurityConfig getSecurityConfig()
        {
            return mockSecurityConfig;
        }
    }

    class AssertingAuthenticator implements Authenticator
    {
        private final boolean loginOK;
        private final String expectedUserName;
        private final String expectedPassword;
        private final AuthenticatorException authenticatorException;

        public AssertingAuthenticator(final boolean loginOK, final String expectedUserName, final String expectedPassword)
        {
            this(loginOK, expectedUserName, expectedPassword, null);
        }

        public AssertingAuthenticator(final boolean loginOK, final String expectedUserName, final String expectedPassword, final AuthenticatorException authenticatorException)
        {
            this.loginOK = loginOK;
            this.expectedUserName = expectedUserName;
            this.expectedPassword = expectedPassword;
            this.authenticatorException = authenticatorException;
        }

        @Override
        public boolean login(final HttpServletRequest request, final HttpServletResponse response, final String username, final String password)
                throws AuthenticatorException
        {
            return loginImpl(username, password);
        }

        @Override
        public boolean login(final HttpServletRequest request, final HttpServletResponse response, final String username, final String password, final boolean storeCookie)
                throws AuthenticatorException
        {
            return loginImpl(username, password);
        }

        private boolean loginImpl(final String username, final String password) throws AuthenticatorException
        {
            assertEquals(expectedUserName, username);
            assertEquals(expectedPassword, password);
            if (authenticatorException != null)
            {
                throw authenticatorException;
            }
            return loginOK;
        }

        @Override
        public String getRemoteUser(final HttpServletRequest request)
        {
            return null;
        }

        @Override
        public void destroy()
        {
        }

        @Override
        public Principal getUser(final HttpServletRequest request)
        {
            return null;
        }

        @Override
        public Principal getUser(final HttpServletRequest request, final HttpServletResponse response)
        {
            return null;
        }

        @Override
        public boolean logout(final HttpServletRequest request, final HttpServletResponse response)
                throws AuthenticatorException
        {
            return false;
        }

        @Override
        public void init(final Map<String, String> params, final SecurityConfig config)
        {
        }
    }

    class CountingLoginInterceptor implements LoginInterceptor
    {
        private int beforeLogin;
        private int afterLogin;

        @Override
        public void beforeLogin(final HttpServletRequest request, final HttpServletResponse response, final String username, final String password, final boolean cookieLogin)
        {
            beforeLogin++;
        }

        @Override
        public void afterLogin(final HttpServletRequest request, final HttpServletResponse response, final String username, final String password, final boolean cookieLogin, final String loginStatus)
        {
            afterLogin++;
        }

        @Override
        public void destroy()
        {
        }

        @Override
        public void init(final Map<String, String> params, final SecurityConfig config)
        {
        }

        public int getBeforeLogin()
        {
            return beforeLogin;
        }

        public int getAfterLogin()
        {
            return afterLogin;
        }
    }

    private HttpServletResponse makeHttpResponse()
    {
        final Mock mockHttpServletResponse = new Mock(HttpServletResponse.class);
        mockHttpServletResponse.expect("addHeader", C.ANY_ARGS);
        return (HttpServletResponse) mockHttpServletResponse.proxy();
    }

    private LocalMockHttpServletRequest makeHttpRequest()
    {
        final LocalMockHttpServletRequest httpServletRequest = new LocalMockHttpServletRequest();
        httpServletRequest.setupGetAttribute(null);

        final HttpSession session = new MockSession();
        httpServletRequest.setSession(session);
        return httpServletRequest;
    }


}
