package com.atlassian.seraph.filter;

import com.atlassian.seraph.auth.AuthenticationContext;
import com.atlassian.seraph.auth.Authenticator;
import com.atlassian.seraph.auth.DefaultAuthenticator;
import com.sun.security.auth.UserPrincipal;
import junit.framework.TestCase;

import static org.mockito.Mockito.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;

public class TestContextAwareAuthenticator extends TestCase
{
    AuthenticationContext mockAuthenticationContext;
    HttpServletRequest mockRequest;
    TestFilter testFilter;
    BaseLoginFilter.SecurityHttpRequestWrapper testSecurityWrapper;
    Principal testPrincipal;

    @Override
    public void setUp() throws Exception
    {
        super.setUp();

        testFilter = new TestFilter();
        testPrincipal = new UserPrincipal("Matt");

        mockAuthenticationContext = mock(AuthenticationContext.class);
        testFilter.setAuthenticationContext(mockAuthenticationContext);

        mockRequest = mock(HttpServletRequest.class);
        testSecurityWrapper = testFilter.getHttpRequestWrapper(mockRequest);

        stub(mockAuthenticationContext.getUser()).toReturn(testPrincipal);
    }

    public void testContextAwareAuthenticator()
    {
        Authenticator authenticator = new AuthenticationContextAwareAuthenticator();
        testFilter.setAuthenticator(authenticator);
        assertEquals("Matt", testSecurityWrapper.getRemoteUser());
    }

    public void testNonContextAwareAuthenticator()
    {
        Authenticator authenticator = new NonAuthenticationContextAwareAuthenticator();
        testFilter.setAuthenticator(authenticator);
        assertEquals("Bob", testSecurityWrapper.getRemoteUser());
    }

    @com.atlassian.seraph.auth.AuthenticationContextAwareAuthenticator
    public class AuthenticationContextAwareAuthenticator extends DefaultAuthenticator
    {
        @Override
        protected boolean authenticate(Principal user, String password)
        {
            return true;
        }

        @Override
        protected Principal getUser(String username)
        {
            return null;
        }

        @Override
        public Principal getUser(HttpServletRequest request)
        {
            return new UserPrincipal("Bob");
        }
    }

    //Note: annotations are not inherited.
    public class NonAuthenticationContextAwareAuthenticator extends AuthenticationContextAwareAuthenticator
    { }


    public class TestFilter extends BaseLoginFilter
    {
        private Authenticator authenticator;
        private AuthenticationContext authenticationContext;

        public SecurityHttpRequestWrapper getHttpRequestWrapper(HttpServletRequest request)
        {
            return new SecurityHttpRequestWrapper(request);
        }

        //We are required to implement this
        @Override
        public String login(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)
        {
            return null;
        }

        public void setAuthenticator(Authenticator authenticator)
        {
            this.authenticator = authenticator;
        }

        @Override
        protected Authenticator getAuthenticator()
        {
            return authenticator;
        }

        @Override
        public AuthenticationContext getAuthenticationContext()
        {
            return authenticationContext;
        }

        public void setAuthenticationContext(AuthenticationContext authenticationContext)
        {
            this.authenticationContext = authenticationContext;
        }
    }
}
