package com.atlassian.seraph.filter;

import com.atlassian.seraph.RequestParameterConstants;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestLoginFilter
{
    private final static String USERNAME = "auser";
    private final static String PASSWORD = "apassword";
    private final static String ENCODING = "UTF-8";

    @Mock
    HttpServletRequest httpServletRequest;

    LoginFilter loginFilter;

    @Before
    public void setUp()
    {
        loginFilter = new LoginFilter();
        when(httpServletRequest.getParameter(RequestParameterConstants.OS_USERNAME))
            .thenReturn(USERNAME);
        when(httpServletRequest.getParameter(RequestParameterConstants.OS_PASSWORD))
            .thenReturn(PASSWORD);
    }

    @Test
    public void testIgnoreRequiresBothPasswordParamAndValue()
    {
        when(httpServletRequest.getParameter(RequestParameterConstants.OS_PASSWORD))
            .thenReturn(null);
        setPasswordParameterInQueryString(PASSWORD);
        assertNotNull(loginFilter.extractUserPasswordPair(httpServletRequest));
    }

    @Test
    public void testIgnoreGetWithPasswordParamByDefault()
    {
        when(httpServletRequest.getMethod()).thenReturn("GET");
        setPasswordParameterInQueryString(PASSWORD);
        assertNull(loginFilter.extractUserPasswordPair(httpServletRequest));
    }

    @Test
    public void testAcceptGetWithPasswordParamAndAllowUrlParameterValueTrue()
    {
        when(httpServletRequest.getMethod()).thenReturn("GET");
        setPasswordParameterInQueryString(PASSWORD);
        loginFilter.setAllowUrlParameterValue(true);
        assertNotNull(loginFilter.extractUserPasswordPair(httpServletRequest));
    }

    @Test
    public void testIgnorePostWithPasswordParamAndUrlParameterByDefault()
    {
        when(httpServletRequest.getMethod()).thenReturn("POST");
        setPasswordParameterInQueryString(PASSWORD);
        assertNull(loginFilter.extractUserPasswordPair(httpServletRequest));
    }

    @Test
    public void testAcceptPostWithPasswordParamValueNotInQueryStringByDefault()
    {
        when(httpServletRequest.getMethod()).thenReturn("POST");
        when(httpServletRequest.getQueryString())
            .thenReturn(urlEncode("something=" + RequestParameterConstants.OS_PASSWORD));
        assertNotNull(loginFilter.extractUserPasswordPair(httpServletRequest));
    }

    @Test
    public void testAcceptPostWithPasswordParamAndUrlParameterWithAllowUrlParameterValueTrue()
    {
        when(httpServletRequest.getMethod()).thenReturn("POST");
        setPasswordParameterInQueryString(PASSWORD);
        loginFilter.setAllowUrlParameterValue(true);
        assertNotNull(loginFilter.extractUserPasswordPair(httpServletRequest));
    }

    @Test
    public void testAcceptNullQueryString()
    {
        when(httpServletRequest.getMethod()).thenReturn("GET");
        assertNotNull(loginFilter.extractUserPasswordPair(httpServletRequest));
    }

    @Test
    public void testWithDisableLoggingDeprecation()
    {
        when(httpServletRequest.getMethod()).thenReturn("GET");
        loginFilter.setAllowUrlParameterValue(true);
        loginFilter.setDisableLoggingDeprecationUrlParameterValue(true);
        setPasswordParameterInQueryString(PASSWORD);
        assertNotNull(loginFilter.extractUserPasswordPair(httpServletRequest));
    }

    private String urlEncode(String string)
    {
        try
        {
            return URLEncoder.encode(string, ENCODING);
        }
        catch (UnsupportedEncodingException e)
        {
            throw new RuntimeException(e);
        }
    }


    private void setPasswordParameterInQueryString(String password)
    {
        when(httpServletRequest.getQueryString())
            .thenReturn(urlEncode(RequestParameterConstants.OS_PASSWORD + "=" + password));
    }

}
