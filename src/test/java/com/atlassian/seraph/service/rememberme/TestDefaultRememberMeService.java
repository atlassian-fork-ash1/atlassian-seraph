package com.atlassian.seraph.service.rememberme;

import java.util.concurrent.TimeUnit;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.seraph.spi.rememberme.RememberMeConfiguration;
import com.atlassian.seraph.spi.rememberme.RememberMeTokenDao;

import org.mockito.ArgumentCaptor;

import junit.framework.TestCase;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 */
public class TestDefaultRememberMeService extends TestCase
{
    private HttpServletRequest request;
    private HttpServletResponse response;
    private RememberMeConfiguration configuration;
    private RememberMeTokenGenerator tokenGenerator;
    private RememberMeTokenDao tokenDao;
    private DefaultRememberMeService service;
    private RememberMeToken fredsToken;

    private static final String FRED_FLINTSTONE = "fred flintstone";
    private static final String RANDOM_STRING = "randomString";
    private static final String COOKIE_NAME = "cookieName";
    private static final String COOKIE_DOMAIN = "cookiedomain";
    private static final String COOKIE_PATH = "cookiepath";
    private static final int COOKIE_AGE = 1673;
    private static final long TOKEN_ID = 456L;
    private static final String COLON = "%3A";
    private static final String COOKIE_VALUE = "456" + COLON + "randomString";
    private static final String BAD_COOKIE_VALUE = "456" + COLON + "badValue";

    @Override
    protected void setUp() throws Exception
    {
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        configuration = mock(RememberMeConfiguration.class);
        tokenGenerator = mock(RememberMeTokenGenerator.class);
        tokenDao = mock(RememberMeTokenDao.class);


        service = new DefaultRememberMeService(configuration, tokenDao, tokenGenerator);

        fredsToken = DefaultRememberMeToken.builder(TOKEN_ID, RANDOM_STRING).setUserName(FRED_FLINTSTONE)
                    .setCreatedTime(System.currentTimeMillis()).build();

        when(tokenGenerator.generateToken(FRED_FLINTSTONE)).thenReturn(fredsToken);
        when(configuration.getCookieName()).thenReturn(COOKIE_NAME);
        when(configuration.getCookieDomain(request)).thenReturn(COOKIE_DOMAIN);
        when(configuration.getCookiePath(request)).thenReturn(COOKIE_PATH);
        when(configuration.getCookieMaxAgeInSeconds()).thenReturn(COOKIE_AGE);
        when(configuration.isCookieHttpOnly(any(HttpServletRequest.class))).thenReturn(false);
    }

    @Override
    protected void tearDown() throws Exception
    {
    }

    public void testAddRememberMe_NoCookies()
    {
        when(request.getCookies()).thenReturn(null);
        when(tokenDao.save(fredsToken)).thenReturn(fredsToken);

        service.addRememberMeCookie(request, response, FRED_FLINTSTONE);

        assertCookieValuesAreSet(response, false);
    }

    public void testAddRememberMe_ZeroCookies()
    {
        when(request.getCookies()).thenReturn(new Cookie[0]);
        when(tokenDao.save(fredsToken)).thenReturn(fredsToken);

        service.addRememberMeCookie(request, response, FRED_FLINTSTONE);

        assertCookieValuesAreSet(response, false);
    }

    public void testAddRememberMe_NoCookies_HttpOnly()
    {
        when(configuration.isCookieHttpOnly(any(HttpServletRequest.class))).thenReturn(true);
        when(request.getCookies()).thenReturn(new Cookie[0]);
        when(tokenDao.save(fredsToken)).thenReturn(fredsToken);

        service.addRememberMeCookie(request, response, FRED_FLINTSTONE);

        assertCookieValuesAreSet(response, true);
    }

    public void testAddRememberMe_WithExistingCookies()
    {
        Cookie existingCookie = new Cookie(COOKIE_NAME, "oldValue");
        when(request.getCookies()).thenReturn(new Cookie[] { existingCookie });
        when(tokenDao.save(fredsToken)).thenReturn(fredsToken);

        service.addRememberMeCookie(request, response, FRED_FLINTSTONE);

        assertCookieValuesAreSet(response, false);
    }

    public void testAddRememberMe_WithExistingCookies_HttpOnly()
    {
        when(configuration.isCookieHttpOnly(any(HttpServletRequest.class))).thenReturn(true);

        Cookie existingCookie = new Cookie(COOKIE_NAME, "oldValue");
        when(request.getCookies()).thenReturn(new Cookie[] { existingCookie });
        when(tokenDao.save(fredsToken)).thenReturn(fredsToken);

        service.addRememberMeCookie(request, response, FRED_FLINTSTONE);

        assertCookieValuesAreSet(response, true);
    }

    public void testRemoveRememberMeCookie_NoCookies()
    {
        when(request.getCookies()).thenReturn(new Cookie[] { });

        service.removeRememberMeCookie(request, response);

        verify(response, times(0)).addHeader(null, null);
    }

    public void testRemoveRememberMeCookie_WithExistingCookie()
    {
        Cookie existingCookie = new Cookie(COOKIE_NAME, "123:ABCD");

        when(request.getCookies()).thenReturn(new Cookie[] { existingCookie });

        service.removeRememberMeCookie(request, response);

        assertThatExistingCookieIsRemoved(false);
        verify(tokenDao).remove(123L);
    }

     public void testRemoveRememberMeCookie_WithExistingCookie_HttpOnly()
    {
        Cookie existingCookie = new Cookie(COOKIE_NAME, "123:ABCD");

        when(request.getCookies()).thenReturn(new Cookie[] { existingCookie });
        when(configuration.isCookieHttpOnly(any(HttpServletRequest.class))).thenReturn(true);

        service.removeRememberMeCookie(request, response);

        assertThatExistingCookieIsRemoved(true);
        verify(tokenDao).remove(123L);
    }

    private void assertCookieValuesAreSet(final HttpServletResponse response, boolean httpOnlyUsed)
    {
        ArgumentCaptor<Cookie> cookieCaptor = ArgumentCaptor.forClass(Cookie.class);
        verify(response, times(1)).addCookie(cookieCaptor.capture());

        final Cookie cookie = cookieCaptor.getValue();
        assertEquals(COOKIE_NAME, cookie.getName());
        assertEquals(COOKIE_DOMAIN, cookie.getDomain());
        assertEquals(COOKIE_PATH, cookie.getPath());
        assertEquals(COOKIE_AGE, cookie.getMaxAge());
        assertEquals(COOKIE_VALUE, cookie.getValue());
        assertEquals(httpOnlyUsed, cookie.isHttpOnly());
    }

    private void assertThatExistingCookieIsRemoved(boolean isHttpOnly)
    {
        ArgumentCaptor<Cookie> cookieCaptor = ArgumentCaptor.forClass(Cookie.class);
        verify(response, times(1)).addCookie(cookieCaptor.capture());

        final Cookie cookie = cookieCaptor.getValue();
        assertEquals(COOKIE_NAME, cookie.getName());
        assertEquals(COOKIE_DOMAIN, cookie.getDomain());
        assertEquals(COOKIE_PATH, cookie.getPath());
        assertEquals(0, cookie.getMaxAge());
        assertEquals("", cookie.getValue());
        assertEquals(isHttpOnly, cookie.isHttpOnly());
    }

    public void testGetRememberMeCookieAuthenticatedUsername_NoCookies()
    {
        when(request.getCookies()).thenReturn(new Cookie[0]);

        final String username = service.getRememberMeCookieAuthenticatedUsername(request, response);
        assertNull(username);
    }

    public void testGetRememberMeCookieAuthenticatedUsername_HasCookie_ButItDoesNotMatch()
    {
        Cookie existingCookie = new Cookie(COOKIE_NAME, BAD_COOKIE_VALUE);
        when(request.getCookies()).thenReturn(new Cookie[] { existingCookie });

        when(tokenDao.findById(TOKEN_ID)).thenReturn(fredsToken);

        final String username = service.getRememberMeCookieAuthenticatedUsername(request, response);
        assertNull(username);

        assertThatExistingCookieIsRemoved(false);
    }


    public void testGetRememberMeCookieAuthenticatedUsername_HasCookie_ButTheAppDoesntLikeIt()
    {
        Cookie existingCookie = new Cookie(COOKIE_NAME, COOKIE_VALUE);
        when(request.getCookies()).thenReturn(new Cookie[] { existingCookie });

        when(tokenDao.findById(TOKEN_ID)).thenReturn(null);

        final String username = service.getRememberMeCookieAuthenticatedUsername(request, response);
        assertNull(username);

        assertThatExistingCookieIsRemoved(false);
    }

    public void testGetRememberMeCookieAuthenticatedUsername_HasCookie_AndItsGood()
    {
        Cookie existingCookie = new Cookie(COOKIE_NAME, COOKIE_VALUE);
        when(request.getCookies()).thenReturn(new Cookie[] { existingCookie });

        when(tokenDao.findById(TOKEN_ID)).thenReturn(fredsToken);

        final String username = service.getRememberMeCookieAuthenticatedUsername(request, response);
        assertEquals(FRED_FLINTSTONE, username);
    }

    public void testGetRememberMeCookieAuthenticatedUsername_expiredToken()
    {
        Cookie existingCookie = new Cookie(COOKIE_NAME, COOKIE_VALUE);
        when(request.getCookies()).thenReturn(new Cookie[] { existingCookie });

        RememberMeToken token = DefaultRememberMeToken.builder(TOKEN_ID, RANDOM_STRING).setUserName(FRED_FLINTSTONE)
            .setCreatedTime(System.currentTimeMillis() - TimeUnit.SECONDS.toMillis(COOKIE_AGE) - 1).build();

        when(tokenDao.findById(TOKEN_ID)).thenReturn(token);

        final String username = service.getRememberMeCookieAuthenticatedUsername(request, response);
        assertNull(username);
        assertThatExistingCookieIsRemoved(false);

    }
}
