package com.atlassian.seraph.service.rememberme;

import junit.framework.TestCase;

/**
 */
public class TestDefaultRememberMeTokenGenerator extends TestCase
{
    private static final String FRED_FLINTSTONE = "Fred Flintstone";

    public void testGenerateToken()
    {
        final DefaultRememberMeTokenGenerator generator = new DefaultRememberMeTokenGenerator();
        final RememberMeToken token = generator.generateToken(FRED_FLINTSTONE);
        assertNotNull(token);
        assertNotNull(token.getRandomString());
        assertEquals(FRED_FLINTSTONE,token.getUserName());

        // these are null at this stage
        assertNull(token.getId());
    }
}
