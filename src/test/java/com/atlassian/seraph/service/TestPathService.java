package com.atlassian.seraph.service;

import junit.framework.TestCase;

/**
 * 
 * @author greg
 * @author $Author: jturner $ (last edit)
 * @version $Revision: 191 $
 */
public class TestPathService extends TestCase {
    public void testParseRolesWorksWithSingleRole() {
        PathService pathService = new PathService();
        String[] result = pathService.parseRoles("blah");
        assertEquals(1, result.length);
        assertEquals("blah", result[0]);
    }

    public void testParseRolesPrefixingAndSuffixingSeparatorsShouldBeDiscarded() {
        PathService pathService = new PathService();
        String[] result = pathService.parseRoles("; blah,foo; ");
        assertEquals(2, result.length);
        assertEquals("blah", result[0]);
        assertEquals("foo", result[1]);
    }

    public void testParseRolesJustAQuoteShouldReturnEmptyArray() {
        PathService pathService = new PathService();
        String[] result = pathService.parseRoles(",");
        assertEquals(0, result.length);
    }

    public void testParseRolesWorksWithExtraSpacesAndIsUserFriendlyEnough() {
        PathService pathService = new PathService();
        String[] result = pathService.parseRoles("blah foo, bar; baz   idiot ");
        assertEquals(5, result.length);
        assertEquals("blah", result[0]);
        assertEquals("foo", result[1]);
        assertEquals("bar", result[2]);
        assertEquals("baz", result[3]);
        assertEquals("idiot", result[4]);
    }

    public void testParseRolesAlsoWorksWithNewLinesAndTabs() {
        PathService pathService = new PathService();
        String[] result = pathService.parseRoles("blah foo, \t\tbar;\n baz   idiot ");
        assertEquals(5, result.length);
        assertEquals("blah", result[0]);
        assertEquals("foo", result[1]);
        assertEquals("bar", result[2]);
        assertEquals("baz", result[3]);
        assertEquals("idiot", result[4]);
    }

    public void testParseRolesWorksWithNoSpacesToo() {
        PathService pathService = new PathService();
        String[] result = pathService.parseRoles("blah,foo,bar,baz,idiot");
        assertEquals(5, result.length);
        assertEquals("blah", result[0]);
        assertEquals("foo", result[1]);
        assertEquals("bar", result[2]);
        assertEquals("baz", result[3]);
        assertEquals("idiot", result[4]);
    }
}
